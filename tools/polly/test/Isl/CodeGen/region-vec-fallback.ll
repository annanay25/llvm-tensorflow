; RUN: opt %loadPolly -polly-vectorizer=polly -polly-codegen -S < %s
;
; Check that we do not crash while attempting to vectorize a region stmt.
;
; CHECK : polly.start
;
source_filename = "bugpoint-output-4e649ba.bc"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: nounwind uwtable
define void @calcfreq_nuc(double* %datafreq) #0 {
entry:
  br label %for.body

for.cond1.preheader:                              ; preds = %for.body
  br label %for.body37

for.body:                                         ; preds = %for.body, %entry
  br i1 undef, label %for.body, label %for.cond1.preheader

for.cond49.preheader:                             ; preds = %for.inc46
  ret void

for.body37:                                       ; preds = %for.inc46, %for.cond1.preheader
  %indvars.iv116 = phi i64 [ 0, %for.cond1.preheader ], [ %indvars.iv.next117, %for.inc46 ]
  %arrayidx39 = getelementptr inbounds double, double* %datafreq, i64 %indvars.iv116
  %cmp40 = fcmp olt double undef, 1.000000e-04
  br i1 %cmp40, label %if.then42, label %for.inc46

if.then42:                                        ; preds = %for.body37
  store double 1.000000e-04, double* %arrayidx39, align 8, !tbaa !1
  br label %for.inc46

for.inc46:                                        ; preds = %if.then42, %for.body37
  %indvars.iv.next117 = add nuw nsw i64 %indvars.iv116, 1
  %exitcond118 = icmp ne i64 %indvars.iv.next117, 4
  br i1 %exitcond118, label %for.body37, label %for.cond49.preheader
}

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 5.0.0 "}
!1 = !{!2, !2, i64 0}
!2 = !{!"double", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
